/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.util.Random;
import util.PDF;

/**
 *
 * @author UDS
 */
public class Su_Solucion_Matrix {

    public static void main(String[] args) throws FileNotFoundException, DocumentException {

        String tabla_resultados = "";
        for (int i = 1; 9 >= i; i++) {
            int n = (int) (Math.pow(3, i));
            byte M[][] = crear(n, n);
            //imprimir(M);
            long inicio = getTiempoEjecucion();
            String indice_filas = getMenFilas(M);
            long fin = getTiempoEjecucion();
            System.out.println("------------------------- Escenario:" + i + " -----------------------------");
            System.out.println("Fila menor Sumatoria:\t" + indice_filas);
            tabla_resultados += i + "\t" + n + "\t" + getMilisegundos(inicio, fin) + "\n";
        }
        System.out.println("Escenario \t Tamaño Matriz \t Tiempo Milisegundos\n");
        System.out.println(tabla_resultados);
        PDF pdf = new PDF("src/pdf/", "resultados_nuevos.pdf");
        //Coloque acá sus nombres
        pdf.crear_Tabla(tabla_resultados, "XXXX Y  ZZZZZ");
    }

    private static String getMenFilas(byte[][] M) {

        return "";
    }

    /*
            ESTOS MÉTODOS NO LOS CAMBIE
     */
    private static long getTiempoEjecucion() {
        return System.nanoTime();
    }

    private static double getMilisegundos(long inicio, long fin) {
        return (fin - inicio) / 1e6;
    }

    private static byte[][] crear(int n, int m) {
        if (m <= 0 || n <= 0) {
            throw new RuntimeException("Error en el tamaño de la matriz");
        }
        Random r = new Random();
        byte M[][] = new byte[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                M[i][j] = (byte) (r.nextInt(100));
            }
        }
        return M;
    }

    private static void imprimir(byte[][] M) {
        String msg = "";
        for (byte v[] : M) {
            for (byte elemento : v) {
                msg += elemento + "\t";
            }
            msg += "\n";
        }
        System.out.println(msg);

    }
}
